import { Github } from "@styled-icons/boxicons-logos/Github"
import { Twitter } from "@styled-icons/boxicons-logos/Twitter"
import { Linkedin } from "@styled-icons/boxicons-logos/Linkedin"
import { Instagram } from "@styled-icons/boxicons-logos/Instagram"

const Icons = {
  Github: Github,
  Linkedin: Linkedin,
  Twitter: Twitter,
  Instagram: Instagram
}

export default Icons