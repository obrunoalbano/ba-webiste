const handle = "obrunoalbano"

const links = [
  {
    label: "Github",
    url: `https://github.com/${handle}`,
  },
  {
    label: "Twitter",
    url: `https://twitter.com/${handle}`,
  },
  {
    label: "Linkedin",
    url: `https://www.linkedin.com/in/brunoalbano/`,
  },
  {
    label: "Instagram",
    url: `https://www.instagram.com/${handle}`,
  },
]

export default links