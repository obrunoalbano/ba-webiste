const links = [
  {
    label: "Home",
    url: "/",
  },
  {
    label: "Sobre Mim",
    url: "/about/",
  },
  {
    label: "Cases",
    url: "/lista-de-websites-no-qual-participei-no-desenvolvimento/",
  }
]

export default links